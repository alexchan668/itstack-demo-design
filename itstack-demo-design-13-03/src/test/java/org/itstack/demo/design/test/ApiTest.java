package org.itstack.demo.design.test;

import lombok.extern.slf4j.Slf4j;
import org.itstack.demo.design.AuthInfo;
import org.itstack.demo.design.entity.ProductVO;
import org.itstack.demo.design.service.ProductService;
import org.junit.Test;

import java.math.BigDecimal;

@Slf4j
public class ApiTest {

    @Test
    public void testCreateProduct(){
        log.info("Starting Create Product...");
        ProductVO productVO = ProductVO.builder()
                .skuId(324353535L)
                .stock(123)
                .price(BigDecimal.valueOf(123.32))
                .skuName("苹果手机iPhone15")
                .picturePath("https://aaa.jd.com/")
                .build();

        AuthInfo res = new ProductService().createProduct(productVO);
        log.info("code:{}, res:{}", res.getCode(), res.getInfo());
    }
}
