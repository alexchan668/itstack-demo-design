package org.itstack.demo.design.entity;


import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品VO类
 */
@Data
@Builder
public class ProductVO {
    private Long skuId;
    private String skuName;
    private String picturePath;
    private BigDecimal price;
    private Integer stock;
}
