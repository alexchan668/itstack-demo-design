package org.itstack.demo.design.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 每个handler的配置
 */
@Data
public class ProductCheckHandlerConfig {

    /**
     * 处理器名称
     */
    private String handlerName;

    /**
     * 下一个处理器
     */
    private ProductCheckHandlerConfig next;

    /**
     * 是否降级
     */
    private Boolean down;
}
