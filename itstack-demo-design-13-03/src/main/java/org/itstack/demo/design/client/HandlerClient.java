package org.itstack.demo.design.client;


import lombok.extern.slf4j.Slf4j;
import org.itstack.demo.design.AuthInfo;
import org.itstack.demo.design.entity.ProductVO;
import org.itstack.demo.design.handler.AbstractCheckHandler;
import org.itstack.demo.design.handler.NullValueCheckHandler;
import org.itstack.demo.design.handler.PriceCheckHandler;
import org.itstack.demo.design.handler.StockCheckHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * 执行处理器链路 一般调用入口
 */
@Slf4j
public class HandlerClient {

    public static AuthInfo executeChain(AbstractCheckHandler checkHandler, ProductVO productVO){
        AuthInfo result = checkHandler.handler(productVO);
        if (!result.isSuccess()){
            log.error("HandlerClient 责任链失败返回: {}", result.getInfo());
            return result;
        }

        return result;
    }

    public static Map<String, AbstractCheckHandler> checkHandlerMap = getHashMap();

    private static Map<String, AbstractCheckHandler> getHashMap() {
        HashMap<String, AbstractCheckHandler> map = new HashMap<>();
        map.put("空值校验处理器", new NullValueCheckHandler());
        map.put("库存校验处理器", new StockCheckHandler());
        map.put("价格校验处理器", new PriceCheckHandler());
        return map;
    }
}
