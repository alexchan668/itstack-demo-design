package org.itstack.demo.design.handler;

import lombok.Data;
import org.itstack.demo.design.AuthInfo;
import org.itstack.demo.design.entity.ProductCheckHandlerConfig;
import org.itstack.demo.design.entity.ProductVO;

import java.util.Objects;

@Data
public abstract class AbstractCheckHandler {

    /**
     * 下一个处理器
     */
    protected AbstractCheckHandler nextHandler;

    /**
     * 处理器的配置
     */
    protected ProductCheckHandlerConfig config;
    public abstract AuthInfo handler(ProductVO productVO);

    /**
     * 执行下一个处理器节点的校验
     * @param productVO
     * @return
     */
    protected AuthInfo next(ProductVO productVO){
        if (Objects.isNull(nextHandler)){
            return AuthInfo.success("审批通过!");
        }

        return nextHandler.handler(productVO);
    }
}
