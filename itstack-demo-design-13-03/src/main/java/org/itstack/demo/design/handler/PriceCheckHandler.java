package org.itstack.demo.design.handler;

import lombok.extern.slf4j.Slf4j;
import org.itstack.demo.design.AuthInfo;
import org.itstack.demo.design.ResultCode;
import org.itstack.demo.design.entity.ProductVO;

import java.math.BigDecimal;

@Slf4j
public class PriceCheckHandler extends AbstractCheckHandler{
    @Override
    public AuthInfo handler(ProductVO productVO) {

        String handlerName = this.config.getHandlerName();
        log.info("{} 开始...", handlerName);

        // 降级处理
        if (config.getDown()) {
            log.warn("{} 已降级, 跳过校验...", handlerName);
            return super.next(productVO);
        }

        if (productVO.getPrice().compareTo(BigDecimal.ZERO) < 0 || productVO.getPrice().compareTo(new BigDecimal(100)) > 0){
            log.error("{} 校验错误, 消息为: {}", handlerName, ResultCode.PRICE_RANGE_ERROR);
            return AuthInfo.fail(ResultCode.PRICE_RANGE_ERROR);
        }

        return super.next(productVO);
    }
}
