package org.itstack.demo.design.handler;

import lombok.extern.slf4j.Slf4j;
import org.itstack.demo.design.AuthInfo;
import org.itstack.demo.design.ResultCode;
import org.itstack.demo.design.entity.ProductVO;

@Slf4j
public class StockCheckHandler extends AbstractCheckHandler{
    @Override
    public AuthInfo handler(ProductVO productVO) {
        String handlerName = this.config.getHandlerName();
        log.info("{} 开始...", handlerName);

        // 降级处理
        if (config.getDown()) {
            log.warn("{} 已降级, 跳过校验...", handlerName);
            return super.next(productVO);
        }

        if (productVO.getStock() < 0 || productVO.getStock() > 10){
            log.error("{} 校验错误, 消息为: {}", handlerName, ResultCode.STOCK_RANGE_ERROR);
            return AuthInfo.fail(ResultCode.STOCK_RANGE_ERROR);
        }

        return super.next(productVO);
    }
}
