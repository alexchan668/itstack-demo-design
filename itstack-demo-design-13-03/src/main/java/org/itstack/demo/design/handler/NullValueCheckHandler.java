package org.itstack.demo.design.handler;

import lombok.extern.slf4j.Slf4j;
import org.itstack.demo.design.AuthInfo;
import org.itstack.demo.design.ResultCode;
import org.itstack.demo.design.entity.ProductVO;

import java.util.Objects;
import java.util.Optional;

/**
 * 校验商品中必填的参数
 */
@Slf4j
public class NullValueCheckHandler extends AbstractCheckHandler{
    @Override
    public AuthInfo handler(ProductVO productVO) {

        String handlerName = this.config.getHandlerName();
        log.info("{} 开始...", handlerName);

        // 降级处理
        if (config.getDown()) {
            log.warn("{} 已降级, 跳过校验...", handlerName);
            return super.next(productVO);
        }

        // 参数校验
        if (Objects.isNull(productVO)){
            log.error("{} 出错，消息为: {}", handlerName, ResultCode.PARAM_NULL_ERROR);
            return AuthInfo.fail(ResultCode.PARAM_SKU_NULL_ERROR);
        }

        if (Objects.isNull(productVO.getSkuId())){
            log.error("{} 出错，消息为: {}", handlerName, ResultCode.PARAM_SKU_NULL_ERROR);
            return AuthInfo.fail(ResultCode.PARAM_SKU_NULL_ERROR);
        }

        if (Objects.isNull(productVO.getPrice())){
            log.error("{} 出错，消息为: {}", handlerName, ResultCode.PARAM_PRICE_NULL_ERROR);
            return AuthInfo.fail(ResultCode.PARAM_PRICE_NULL_ERROR);
        }

        if (Objects.isNull(productVO.getStock())){
            log.error("{} 出错，消息为: {}", handlerName, ResultCode.PARAM_STOCK_NULL_ERROR);
            return AuthInfo.fail(ResultCode.PARAM_STOCK_NULL_ERROR);
        }

        return super.next(productVO);
    }
}
