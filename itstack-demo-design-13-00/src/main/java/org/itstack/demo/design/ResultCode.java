package org.itstack.demo.design;

public enum ResultCode {
    PARAM_NULL_ERROR("PARAM_NULL_ERROR", "参数实体类为空"),
    PARAM_SKU_NULL_ERROR("PARAM_SKU_NULL_ERROR", "参数的SKU为空"),
    PARAM_STOCK_NULL_ERROR("PARAM_STOCK_NULL_ERROR", "参数的库存为空"),
    PARAM_PRICE_NULL_ERROR("PARAM_PRICE_NULL_ERROR", "参数的价格为空"),
    STOCK_RANGE_ERROR("PRICE_RANGE_ERROR", "库存范围出错!"),
    PRICE_RANGE_ERROR("PRICE_RANGE_ERROR", "价格范围出错!");

    final String code;
    final String msg;

    ResultCode(String code, String msg){
        this.code = code;
        this.msg = msg;
    }
    String getCode(){
        return this.code;
    }

    String getMsg(){
        return this.msg;
    }

}
