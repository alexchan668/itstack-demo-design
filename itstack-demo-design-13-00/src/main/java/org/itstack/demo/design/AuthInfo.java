package org.itstack.demo.design;

public class AuthInfo {

    private Integer code;
    private String info = "";

    public AuthInfo(Integer code, String ...infos) {
        this.code = code;
        for (String str:infos){
            this.info = this.info.concat(str);
        }
    }

    public AuthInfo(String code, String ...infos) {
        this.code = Integer.parseInt(code);
        for (String str:infos){
            this.info = this.info.concat(str);
        }
    }

    public static AuthInfo success(String ...infos){
        return new AuthInfo(200, infos);
    }

    public static AuthInfo fail(Integer code, String ...infos){
        return new AuthInfo(code, infos);
    }

    public static AuthInfo fail(ResultCode code){
        return new AuthInfo("-1", code.getMsg());
    }
    public static AuthInfo fail(String ...infos){
        return new AuthInfo("-1", infos);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isSuccess() {
        return 200 == code;
    }
}
